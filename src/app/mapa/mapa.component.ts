import { Component, OnInit } from '@angular/core';

declare let L;

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.sass']
})
export class MapaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    const map = L.map('map').setView([38.06936861859707, -1.1708539724349973], 13);
    
    /** Agregamos una capa base al mapa */
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      maxZoom: 18,
      minZoom: 9
    }).addTo(map);
    
    /** Agregamos un círculo al mapa  */
    let circle = L.circle([38.06936861859707, -1.1708539724349973],{
      color: 'red',
      fillColor: '#192a3d',
      fillOpacity: 0.3,
      radius: 100, // 100 metros
    }).addTo(map);

    /** Agregamos un polígono al mapa */
    var polygon = L.polygon(
      [
        [
          38.0708826263759,
          -1.1727261543273926
        ],
        [
          38.07012245902307,
          -1.1733698844909668
          
        ],
        [
          38.07037584901842,
          -1.1715888977050781
        ],
        [
          38.0708826263759,
          -1.1727261543273926
        ]
      ]      
      ).addTo(map);

    /** Añadimos un popUp a las figuras de antes */
    circle.bindPopup('<b>Hello World!</b><br>Aquí viven los Roldán Váquez del Imperio!!');
    polygon.bindPopup('<b>Hello World!</b><br>Soy un popUp!');

    /** Añadimos un marker con popUp al mapa */    
    var marker = L.marker([38.06936861859707, -1.1708539724349973]).addTo(map);
    marker.bindPopup('<b>Hello World!</b><br>Justo aquí!!');
    
    /** Añadimos un popUp independiente */
    let popUp = L.popup()
                .setLatLng([38.07216644663465, -1.1674904823303223])
                .setContent("Soy un campo de Golf")
                .openOn(map);
    
    /** Añadimos una función */
    let popUp2 = L.popup();
    
    function onMapClick(e){
      popUp2
        .setLatLng(e.latlng)
        .setContent("<b>Has pulsado en</b><br>" + e.latlng.toString())
        .openOn(map);
    }
    map.on('click', onMapClick);
  }

}
